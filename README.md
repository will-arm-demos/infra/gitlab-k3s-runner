[[_TOC_]]

# Helm

Used for deploying mostly the GitLab Runner helm chart.

# Use

Have helm installed on the local system and have the `kube config` set. 

# Installing GitLab Runner

Initialize helm repo (onetime setup): https://docs.gitlab.com/runner/install/kubernetes.html

Simply run `./install_runner.sh lug gl-runner.toml`. 

# Removing Runner

To remove and unregister the runner, run `./remote_runner.sh lug`.
